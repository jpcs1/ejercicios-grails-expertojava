package es.ua.expertojava.todo

class Category {

    String name
    String description

    static hasMany = [todos:Todo]

    static constraints = {
        name(blank:false)
        description(blank:true, nullable:true, maxSize:1000)
    }

    String toString(){
        name
    }
    /*********
    def EliminarDesdeTodos(Todo tarea)
    {
        todos.remove(tarea)
    }
    /**********/
}
