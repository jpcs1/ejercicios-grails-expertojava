package es.ua.expertojava.todo

class TodoTagLib {
    static defaultEncodeAs = [taglib:'None']
    //static encodeAsForTags = [tagName: [taglib: 'html'],otherTagName:[taglib: 'None']]

    static namespace = 'todo'

    def printIconFromBoolean = { attrs ->
        if(attrs.value) {
            out << asset.image(src: "iconTrue.png")
        }else {
            out <<  asset.image(src: "iconFalse.png")
        }
    }
}
