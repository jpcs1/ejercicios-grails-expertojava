package es.ua.expertojava.todo

import grails.transaction.Transactional

import static org.springframework.http.HttpStatus.NO_CONTENT


class CategoryService {

    def deleteCategory(Category categoryInstance) {

        def todos = categoryInstance.todos.findAll()

        todos.each {todo ->
            categoryInstance.removeFromTodos(todo)
        }
        categoryInstance.delete flush:true

    }
}