package es.ua.expertojava.todo


class TodoService {

    def deleteTodo(Todo todoInstance) {

        def tags = todoInstance.tags.findAll()
        tags.each {tag ->
            todoInstance.removeFromTags(tag)
        }

        todoInstance.delete flush:true
    }

    def saveTodo(Todo todoInstance, User user) {

        todoInstance.user = user
        todoInstance.save()

        if(todoInstance.done)
        {
            todoInstance.dateDone = todoInstance.dateCreated
        }

        if (todoInstance.hasErrors()) {
            return true
        }

        todoInstance.save flush:true

        return false
    }

    def updateTodo(Todo todoInstance) {

        todoInstance.save()
        if(todoInstance.done)
        {
            todoInstance.dateDone = todoInstance.lastUpdated
        }

        if (todoInstance.hasErrors()) {
            return true
        }

        todoInstance.save flush:true

        return false
    }

    def lastTodosDone(Integer hours) {
        Date now = new Date()
        Calendar calendar = Calendar.getInstance()
        calendar.setTime(now)
        calendar.add(Calendar.HOUR_OF_DAY, -hours)
        Date to = calendar.getTime()
        Todo.findAllByDateDoneBetween(to, now)
    }

    def getNextTodos(Integer days, params) {
        Date now = new Date(System.currentTimeMillis())
        Date to = now + days
        Todo.findAllByDateBetween(now, to, params)
    }
    def countNextTodos(Integer days) {
        Date now = new Date(System.currentTimeMillis())
        Date to = now + days
        Todo.countByDateBetween(now, to)
    }
}