<header id="menu">
    <nobr>
        <g:if test="${isUserLoggedIn}">
            <b>${userInstance?.name} ${userInstance?.surnames}</b> |
            <g:link controller="user" action="logout">Logout</g:link>
        </g:if>
        <g:else>
            <g:link controller="user" action="login">Login</g:link>
        </g:else>
    </nobr>
</header>

