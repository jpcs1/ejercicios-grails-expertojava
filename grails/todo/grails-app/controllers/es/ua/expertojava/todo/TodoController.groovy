package es.ua.expertojava.todo

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TodoController {

    def todoService
    def springSecurityService
    def user

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        user = springSecurityService.currentUser

        params.max = Math.min(max ?: 10, 100)

        respond Todo.findAllByUser( user, params ), model:[todoInstanceCount: Todo.count()]
    }

    def search = {
        def query = params.q
        if(query){

            //def todos = Todo.searchEvery(query)

            /* Probando el filtrado por usuario*/
            def todos = Todo.search ([result: 'every']) {
                must(queryString(query))
                must(term('$/Todo/user/id', User.findByUsername(user.username)?.id))
            }

            def todosCurrentUser = []

            for (t in todos) {
                if(t.user.username == user.username) {
                    todosCurrentUser.add(t)
                }
            }

            render(view: "index",
                    model: [todoInstanceList: todosCurrentUser.toList(),
                            todoInstanceCount:todosCurrentUser.size()])
        }else{
            redirect(action: "index")
        }
    }

    def show(Todo todoInstance) {
        respond todoInstance
    }

    def create() {
        respond new Todo(params)
    }

    @Transactional
    def save(Todo todoInstance) {

        if (todoInstance == null) {
            notFound()
            return
        }

        def error = todoService.saveTodo(todoInstance, springSecurityService.currentUser)

        if (error) {
            respond todoInstance.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'todo.label', default: 'Todo'), todoInstance.title])
                redirect todoInstance
            }
            '*' { respond todoInstance, [status: CREATED] }
        }
    }

    def edit(Todo todoInstance) {
        respond todoInstance
    }

    @Transactional
    def update(Todo todoInstance) {

        if (todoInstance == null) {
            notFound()
            return
        }

        def error = todoService.updateTodo(todoInstance)

        if (error) {
            respond todoInstance.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Todo.label', default: 'Todo'), todoInstance.title])
                redirect todoInstance
            }
            '*'{ respond todoInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Todo todoInstance) {

        if (todoInstance == null) {
            notFound()
            return
        }

        todoService.deleteTodo(todoInstance)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Todo.label', default: 'Todo'), todoInstance.title])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'todo.label', default: 'Todo'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def categories(Todo todoInstance) {
        respond todoInstance
    }


    def showTodosByUser(){
        def username = params.username

        User usuario = User.findByName(username)

        respond Todo.findAllByUser(usuario, params), model:[todoInstanceCount: Todo.count()]
    }

    def listNextTodos(Integer days) {
        respond todoService.getNextTodos(days, params),
                model:[todoInstanceCount: todoService.countNextTodos(days)], view: "index"
    }

}