
def factorial

factorial = { it > 1 ? it * factorial(it -1) : it}

println factorial(5)

def lista = [1,2,3,4,5]

lista.each{num -> println factorial(num)}

def ayer
ayer = { fecha ->
return fecha-1
}

def maniana
maniana = { fecha ->
return fecha+1
}

def fechas = [new Date().parse("d/M/yyyy H:m:s","28/6/2008 00:30:20"),new Date().parse("d/M/yyyy H:m:s","28/6/2008 00:30:20"),new Date().parse("d/M/yyyy H:m:s","28/6/2008 00:30:20")]

fechas.each{dia -> println ayer(dia)
            println maniana(dia)}