/**java.lang.String.moneda ={

}
/***/
def moneda(String locale)
{
    if(locale == "en_EN")
    {
        return "£"
    }
    
    if(locale == "en_US")
    {
        return "\$"
    }
    
    if(locale == "es_ES")
    {
        return "€"
    }
}

BigDecimal.metaClass.moneda = { locale ->
    if(locale == "en_EN" || locale == "en_US")
    {
        return moneda(locale) + "${delegate}"
    }
    else
    {
        return "${delegate}" + moneda(locale)
    }
}


assert 10.2.moneda("en_EN") ==  "£10.2"
assert 10.2.moneda("en_US") ==  "\$10.2"
assert 10.2.moneda("es_ES") ==  "10.2€"

Integer.metaClass.moneda = { locale ->
    if(locale == "en_EN" || locale == "en_US")
    {
        return moneda(locale) + "${delegate}"
    }
    else
    {
        return "${delegate}" + moneda(locale)
    }
}


assert 10.moneda("en_EN") ==  "£10"
assert 10.moneda("en_US") ==  "\$10"
assert 10.moneda("es_ES") ==  "10€"

Float.metaClass.moneda = { locale ->
    if(locale == "en_EN" || locale == "en_US")
    {
        return moneda(locale) + "${delegate}"
    }
    else
    {
        return "${delegate}" + moneda(locale)
    }
}


assert new Float(10.2).moneda("en_EN") ==  "£10.2"
assert new Float(10.2).moneda("en_US") ==  "\$10.2"
assert new Float(10.2).moneda("es_ES") ==  "10.2€"


Double.metaClass.moneda = { locale ->
    if(locale == "en_EN" || locale == "en_US")
    {
        return moneda(locale) + "${delegate}"
    }
    else
    {
        return "${delegate}" + moneda(locale)
    }
}

assert new Double(10.2).moneda("en_EN") ==  "£10.2"
assert new Double(10.2).moneda("en_US") ==  "\$10.2"
assert new Double(10.2).moneda("es_ES") ==  "10.2€"