class Libro {
    /* Añade aquí la definición de la clase */
    def nombre
    def anyo
    def autor
    def editorial
    

    
    String getAutor()
    {
        def aux =this.autor.tokenize(",")
        return "${aux.get(1).trim()}" + " " + "${aux.get(0).trim()}"
        
    }
    

}

/* Crea aquí las tres instancias de libro l1, l2 y l3 */
def l1 = new Libro(nombre:'La colmena',anyo:1951,autor:'celia Trulock');
def l2 = new Libro(nombre:'La galatea',anyo:1585,autor:'de Cervantes Saavedra, Miguel');
def l3 = new Libro(nombre:'La dorotea',anyo:1632,autor:'Lope de Vega y Caprio, Félix Arturo');


assert l1.getNombre() == 'La colmena'
assert l2.getAnyo() == 1585
assert l3.getAutor() == 'Félix Arturo Lope de Vega y Caprio'

/* Añade aquí la asignación de la editorial a todos los libros */

l1.setEditorial('Anaya')
l2.setEditorial('Planeta')
l3.setEditorial('Santillana')

assert l1.getEditorial() == 'Anaya'
assert l2.getEditorial() == 'Planeta'

assert l3.getEditorial() == 'Santillana'
