class Todo {
    def titulo;
    def descripcion;
    
    Todo(String tit, String des)
    {
        this.titulo=tit;
        this.descripcion=des;
    }
}

def todos= [new Todo("Lavadora","Poner lavadora"), new Todo("Impresora","Comprar cartuchos")]

todos.each{ 
println "${it.getTitulo()} ${it.getDescripcion()}"
}

return