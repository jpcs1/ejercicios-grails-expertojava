package controllers;

import models.Person;
import play.*;
import play.mvc.*;

import views.html.*;

import javax.xml.transform.Result;
import java.util.List;

import static play.libs.Json.toJson;

public class Application extends Controller {

    public Result index() {
        return ok(index.render("Mi applicacion"));
    }

    public  static  Result addPerson() {
        Person person = Form.formPerson(Person.class).bindFormRequest().get();
        person.save();
        return redirect(routes.Application.index());
    }


    public  static Result getPersons() {
        List<Person> persons = new Model.Finder(String.class, Person.class).all();
        return ok(toJson(persons));
    }

}
